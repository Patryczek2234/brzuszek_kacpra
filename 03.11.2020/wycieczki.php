<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Wycieczki i urlopy</title>
    <link rel="stylesheet" href="styl3.css">
</head>
<body>
    <section class="baner">
        <h1>Biuro podróży</h1>
    </section>
    <section class="lewy">
        <h2>Kontakt</h2>
        <a href="biuro@wycieczki.pl">Napisz do nas</a>
        <p>Telefon: 555 666 777</p>
    </section>
    <section class="srodkowy">
        <h2>Galeria</h2>
        <?php
        $polacz=mysqli_connect("localhost","root","","biuropodrozy");
        ?>

        <?php
        $sql="SELECT `nazwaPliku`,`podpis` FROM `zdjecia` GROUP BY `podpis` ORDER BY `podpis`";
        $result=mysqli_query($polacz,$sql);
        while($row=mysqli_fetch_array($result)){
        echo "<img src=".$row['nazwaPliku']." alt=".$row['podpis'].">";
        }
        ?>
    </section>
    <section class="prawy">
        <h2>Promocje</h2>
        <table>
            <tr>
                <td>Jesień</td>
                <td>Grupa 4+</td>
                <td>Grupa 10+</td>
            </tr>
            <tr>
                <td>5%</td>
                <td>10%</td>
                <td>15%</td>
            </tr>
        </table>
    </section>
    <section class="dane">
        <h2>LISTA WYCIECZEK</h2>
        <?php
        $sql1="SELECT `id`,`dataWyjazdu`,`cel`,`cena` FROM `wycieczki` WHERE `dostepna`='1'";
        $result1=mysqli_query($polacz,$sql1);
        while($row1=mysqli_fetch_array($result1)) {
        echo $row1['id'].",".$row1['dataWyjazdu'].",".$row1['cel'].",cena: ".$row1['cena']."<br>";
        }
        ?>

    </section>
    <section class="stopka">
        <p>Stonę wykonał: Patryk</p>
    </section>
        <?php
        mysqli_close($polacz);
        ?>
</body>
</html>