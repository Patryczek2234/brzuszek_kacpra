<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Tabelka PHP</title>
    <?php
    // Połączenie z bazą danych 
    $polaczenie = mysqli_connect("localhost","root","","tabela_dane") or die("Błąd połączenia");
    mysqli_query($polaczenie, "SET CHARSET utf8");
    ?>
</head>
<body>
<h1>Tabela nr 1</h1>
   <table>
       <tr>
           <td>12</td>
           <td>13</td>
           <td>14</td>
           <td>234</td>
       </tr>
       <tr>
           <td>23423</td>
           <td>2</td>
           <td>1</td>
           <td>3</td>
       </tr>
       <tr>
           <td>3498575347</td>
           <td>345435</td>
           <td>12534</td>
           <td>787653</td>
       </tr>
   </table>
   <h1>Tabela nr 2 </h1>
   <?php
    /** 
     * komentarz wieloliniowy - tworzenie zapytania, poprzez pobieranie wszystkich pracownikow bazy.
    */
    $sql = "SELECT * FROM pracownicy";
    // tworzenie zapytania
    $result = mysqli_query($polaczenie,$sql);
echo "<table>";
echo "<tr>";
   echo "<td>";
      echo "imie";
    echo "</td>";
    echo "<td>";
      echo "nazwisko";
    echo "</td>";
    echo "<td>";
      echo "wiek";
    echo "</td>";
    echo "<td>";
      echo "stanowisko";
    echo "</td>";
    echo "</tr>";
    while($row = mysqli_fetch_array($result)) {
        echo "<td>";
            echo $row["imie"];
        echo "</td>";
        echo "<td>";
            echo $row["nazwisko"];
        echo "</td>";
        echo "<td>";
            echo $row["wiek"];
        echo "</td>";
        echo "<td>";
            echo $row["stanowisko"];
        echo "</td>";
    echo "</tr>";
    }
    echo "</table>"
    ?>
</body>
</html>