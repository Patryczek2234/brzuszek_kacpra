Witam, 
Aktualnie abyście bardziej poznali interfejs inkscape’a przygotowałem wam na podstawie dostępnych arkuszy zadania związane stricte z grafiką wektorową. Generalnie będziemy bazujemy na operacjach między dwoma obiektami. 

1. Punktem wyjścia będzie zapoznanie się z operacjami jakie możemy dokonywać na dwóch obiektach zatem poniżej macie pokazane gdzie możecie znaleźć opcje na obiektach
http://mages.pl/zscku/3ti/1.png

2. Następnie bazując na tym zróbcie dwa dowolne kształty i wykonajcie operacje wg poniższej grafiki
http://mages.pl/zscku/3ti/2.png
Wynikowo zapiszcie ten projekt w 2 plikach .svg lub .pdf
3. Kolejno wykonajcie grafikę bazując na łączeniu prostych obiektów:
http://mages.pl/zscku/3ti/3.png
Do postaci wyjściowej:
http://mages.pl/zscku/3ti/4.png
Podobnie jak powyżej również zapiszcie jako .svg lub .pdf

4. Analogicznie w przypadku poniższych 3 przykładów:
http://mages.pl/zscku/3ti/5.png
http://mages.pl/zscku/3ti/6.png
http://mages.pl/zscku/3ti/7.png
Tutaj również do każdego z projektu proszę o plik .svg lub .pdf

Czas na realizację: do 8/5/20