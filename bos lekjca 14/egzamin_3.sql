-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Czas generowania: 02 Lis 2020, 10:51
-- Wersja serwera: 10.4.11-MariaDB
-- Wersja PHP: 7.2.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `egzamin 3`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `wycieczki`
--

CREATE TABLE `wycieczki` (
  `id` int(11) NOT NULL,
  `zdjencia_id` int(11) NOT NULL,
  `dataWyjazdu` date NOT NULL,
  `dataPrzyjazdu` date DEFAULT NULL,
  `cel` text COLLATE utf8_polish_ci NOT NULL,
  `cena` double NOT NULL,
  `dostepna` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `wycieczki`
--

INSERT INTO `wycieczki` (`id`, `zdjencia_id`, `dataWyjazdu`, `dataPrzyjazdu`, `cel`, `cena`, `dostepna`) VALUES
(1, 1, '2020-10-12', NULL, 'Tokyo', 1000, 1),
(2, 2, '2020-10-06', NULL, 'Warszawa', 120, 1),
(3, 1, '2020-09-15', NULL, 'Tokyo', 1300, 0),
(4, 2, '2020-09-16', NULL, 'Warszawa', 100, 0),
(5, 3, '2020-10-23', NULL, 'Wenecja', 1200, 1),
(6, 3, '2020-09-22', NULL, 'Wenecja', 1400, 0),
(7, 4, '2020-10-30', NULL, 'Paryż', 1300, 1),
(8, 4, '2020-10-22', NULL, 'Paryż', 1200, 1),
(9, 5, '2020-11-09', NULL, 'Barcelona', 1400, 1),
(10, 5, '2020-09-16', NULL, 'Barcelona', 1300, 0);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `zdjecia`
--

CREATE TABLE `zdjecia` (
  `id` int(11) NOT NULL,
  `nazwaPliku` text COLLATE utf8_polish_ci NOT NULL,
  `podpis` text COLLATE utf8_polish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `zdjecia`
--

INSERT INTO `zdjecia` (`id`, `nazwaPliku`, `podpis`) VALUES
(1, 'Tokyo', 'Japonia'),
(2, 'Warszawa', 'Polska'),
(3, 'Wenecja', 'Włochy'),
(4, 'Francja', 'Paryż'),
(5, 'Barcelona', 'Hiszpania');

--
-- Indeksy dla zrzutów tabel
--

--
-- Indeksy dla tabeli `wycieczki`
--
ALTER TABLE `wycieczki`
  ADD PRIMARY KEY (`id`),
  ADD KEY `zdjencia_id` (`zdjencia_id`);

--
-- Indeksy dla tabeli `zdjecia`
--
ALTER TABLE `zdjecia`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `wycieczki`
--
ALTER TABLE `wycieczki`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT dla tabeli `zdjecia`
--
ALTER TABLE `zdjecia`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Ograniczenia dla zrzutów tabel
--

--
-- Ograniczenia dla tabeli `wycieczki`
--
ALTER TABLE `wycieczki`
  ADD CONSTRAINT `wycieczki_ibfk_1` FOREIGN KEY (`zdjencia_id`) REFERENCES `zdjecia` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
