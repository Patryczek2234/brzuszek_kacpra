<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Gromady kręgowców</title>
    <link rel="stylesheet" href="./style12.css">
</head>
<body>
    <section class="gora">
        <section class="menu">
            <a href="./gromada-ryby.html">Gromada ryby</a>
            <a href="./gromada-ptaki.html">Gromada ptaki</a>
            <a href="./gromada-ssaki.html">Gromada ssaki</a>
        </section>
        <section class="logo">
            <h2>GROMADY KRĘGOWCÓW</h2>
        </section>
    </section>
    <section class="glowny">
    <section class="lewy">
        <?php
        $polacz = mysqli_connect("localhost","root","","baza");
        ?>
        <?php
        $sql = "SELECT `id`,`Gromady_id`,`gatunek`,`wystepowanie` FROM `zwierzeta` WHERE `Gromady_id` = '4' OR `Gromady_id` = '5'";
        $result = mysqli_query($polacz, $sql);
        while($row = mysqli_fetch_array($result)) {
            echo "<p>".$row['id'].". ".$row['gatunek']."</p>";
            echo "<p> Występowanie: ".$row['wystepowanie'].",gromada ".$row['Gromady_id']."</p>" ;
            echo "<hr>";
        }
        ?>
        </section>
        <section class="prawy">
            <h1>PTAKI</h1>
            <ol>
                <?php
                $sql2="SELECT `gatunek`,`obraz` FROM `zwierzeta` WHERE `Gromady_id`='4'";
                $result2 = mysqli_query($polacz, $sql2);
                while($row2=mysqli_fetch_array($result2)) {
                    echo "<li><a href=".$row2['obraz'].">".$row2['gatunek']."</a></li>";
                }
                ?>
            </ol>
            <img src="./sroka.jpg" alt="Sroka zwyczajna, gromada ptak">
    </section>
    </section>
    <section class="stopka">
        <p>Stronę o kręgowcach przygotował: Patryk</p>
    </section>
    <?php
    mysqli_close($polacz);
    ?>
</body>
</html>